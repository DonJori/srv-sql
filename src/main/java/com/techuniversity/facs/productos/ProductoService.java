package com.techuniversity.facs.productos;

import com.techuniversity.facs.facturas.FacturaModel;
import com.techuniversity.facs.facturas.FacturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    FacturaRepository facturaRepository;
    @Autowired
    ProductoRepository productoRepository;

    @Transactional(rollbackOn = {Exception.class})
    public void crearFactura(String cliente, int idProducto) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = sdf.format(new Date());

        Optional<ProductoModel> optProd = productoRepository.findById(idProducto);
        if (optProd.isPresent()) {
            ProductoModel producto = optProd.get();
            FacturaModel factura = new FacturaModel();

            factura.setCliente(cliente);
            factura.setFecha(fecha);
            factura.setIdProducto(idProducto);
            factura.setImporte(producto.getPrecio());

            facturaRepository.save(factura);

            int totalStock = producto.getStock() - 1;
            if (totalStock < 0) {
                throw new Exception("Stock < 0");
            }

            producto.setStock(totalStock);
            productoRepository.save(producto);
        } else {
            throw new Exception("No se encontro el producto.");
        }
    }
}
