package com.techuniversity.facs.facturas;

import javax.persistence.*;

@NamedQuery(name = "FacturaModel.findByImporte", query = "from com.techuniversity.facs.facturas.FacturaModel where importe > ?1")
@NamedNativeQuery(name = "FacturaModel.findByFecha", query = "SELECT * FROM facturas a WHERE date(a.fecha) <= date(?1)", resultClass = FacturaModel.class)
@Entity(name = "facturas")
public class FacturaModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String fecha;
    private String cliente;
    private int idProducto;
    private double importe;

    public FacturaModel() {}

    public FacturaModel(int id, String fecha, String cliente, int idProducto, double importe) {
        this.id = id;
        this.fecha = fecha;
        this.cliente = cliente;
        this.idProducto = idProducto;
        this.importe = importe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }
}
